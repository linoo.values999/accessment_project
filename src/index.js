import React from 'react'
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import './index.css'
import 'antd/dist/antd.min.css'
import Layout from './components/Layout'
import Login from './pages/login'
import Register from './pages/register'
import Product from './pages/product'
import { store } from './redux/store'
import { Provider } from 'react-redux'
import {
	BrowserRouter,
	Routes,
	Route,
} from "react-router-dom";
import RequireAuth from './components/RequireAuth';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
	<Provider store={store}>
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<Layout />} >
					<Route path="/" element={<RequireAuth>
						<Product />
					</RequireAuth>} />
					<Route path="/login" element={<Login />} />
					<Route path="/register" element={<Register />} />
				</Route>
			</Routes>
		</BrowserRouter>
	</Provider>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
