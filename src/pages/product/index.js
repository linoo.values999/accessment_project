/* eslint-disable react-hooks/exhaustive-deps */
import { UnorderedListOutlined, DollarOutlined } from '@ant-design/icons';
import { Card, Col, Row, Image, Spin, Button, Space } from "antd";
import { ShoppingCartOutlined } from '@ant-design/icons';
import { useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux'
import { FetchProduct } from '../../redux/reducers/product'
import { addToCart } from '../../redux/slices/cart'
import styles from './index.module.less';
import logo from '../../notFound.png';

const colors = [
    "rgba(168, 0, 48, 0.75)",
    "rgba(0, 168, 160, 0.75)",
    "rgba(0, 168, 78, 0.75)",
    "rgba(212, 162, 36, 0.75)"
];

const Product = () => {
    const dispatch = useDispatch();

    const { searchName, list, loading } = useSelector((state) => state.productReducer);
    const { items } = useSelector((state) => state.cartReducer);

    useEffect(() => {
        dispatch(FetchProduct({ search: searchName }));
    }, [searchName])

    const onError = (event) => {
        event.target.src = logo;
    }

    const getCount = (id) => {
        return items.find(x => x.data.id === id)?.count
    }

    return (
        <>
            <Spin spinning={loading}>
                <div className={styles.container}>
                    <h2>Customers also bought</h2>
                    <Row className={styles.row}>
                        {list.map((item, index) => (
                            <Col xs={24} sm={12} md={6} key={item.id}>
                                <Card
                                    className={styles.card}
                                    bodyStyle={{
                                        padding: 15,
                                        color: "white",
                                        position: "absolute",
                                        bottom: 0,
                                        left: 0,
                                        right: 0,
                                        backgroundColor: colors[index % 4],
                                    }}
                                    bordered={false} hoverable
                                    cover={<Image src={item.image} onError={onError} />}
                                >
                                    <Space direction='vertical' style={{ display: "flex" }}>
                                        <div className={styles.flexBetween}>
                                            <span className={styles.white}>{item.name}</span>
                                            <span>
                                                <ShoppingCartOutlined /> {getCount(item.id) || 0}
                                            </span>
                                        </div>
                                        <span className={styles.white}><UnorderedListOutlined /> {item.category.name}</span>
                                        <div className={styles.flexBetween}>
                                            <span className={styles.white}><DollarOutlined /> {item.amount} MMK</span>
                                            <Button size="small" onClick={() => dispatch(addToCart(item))}>
                                                Add To Cart
                                            </Button>
                                        </div>
                                    </Space>
                                </Card>
                            </Col>
                        ))}
                    </Row>
                </div>
            </Spin>
        </>
    );
}

export default Product;