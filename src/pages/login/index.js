import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { SignIn } from '../../redux/reducers/auth'
import styles from './login.module.less'
import { Form, Input, Button } from 'antd';
import { Link, useNavigate, useLocation } from 'react-router-dom';

const Login = () => {

    const { loading } = useSelector((state) => state.authReducer);

    const initialValues = {
        username: "",
        password: "",
    }

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const location = useLocation()

    let from = location.state?.from?.pathname || "/";

    const changeRoute = () => {
        navigate(from, { replace: true });
    }

    const onFinish = (values) => {
        dispatch(SignIn(values, changeRoute));
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className={styles.title}>
            <h2>Login</h2>
            <div className={styles.form}>
                <Form
                    name="basic"
                    initialValues={initialValues}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input placeholder='User Name' />
                    </Form.Item>

                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password placeholder='Password' />
                    </Form.Item>
                    <Button loading={loading} type="primary" htmlType="submit" block>
                        Submit
                    </Button>
                </Form>
                <Link to="/register" >Register</Link>
            </div>
        </div>
    );
};

export default Login;