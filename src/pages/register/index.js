import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Register } from '../../redux/reducers/auth'
import styles from './login.module.less'
import { Form, Input, Button } from 'antd';
import { Link, useNavigate } from 'react-router-dom';

const RegisterPage = () => {

    const { loading } = useSelector((state) => state.authReducer);
    const navigate = useNavigate();

    const initialValues = {
        username: "",
        password: "",
        confirm: "",
    }

    const dispatch = useDispatch()

    const changeRoute = () => {
        navigate("/", { replace: true });
    }

    const onFinish = (values) => {
        dispatch(Register(values, changeRoute));
    };


    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className={styles.title}>
            <h2>Register</h2>
            <div className={styles.form}>
                <Form
                    name="basic"
                    initialValues={initialValues}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your username!',
                            },
                        ]}
                    >
                        <Input placeholder='User Name' />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password placeholder='Password' />
                    </Form.Item>
                    <Form.Item
                        name="confirm"
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Please confirm your password!',
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }

                                    return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password placeholder='Confirm Password' />
                    </Form.Item>
                    <Button loading={loading} type="primary" htmlType="submit" block>
                        Submit
                    </Button>
                </Form>
                <Link to="/login" >Login</Link>
            </div>
        </div>
    );
};

export default RegisterPage;