import React from 'react';
import { Button, InputNumber, List, Space, Empty } from 'antd';
import { removeFromCart, addMoreItem } from '../../redux/slices/cart';
import { useSelector, useDispatch } from 'react-redux';
import { UnorderedListOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import styles from './index.module.less';

const Cart = () => {
    const { items, subtotal } = useSelector((state) => state.cartReducer);
    const dispatch = useDispatch();

    const onChange = (value, id) => {
        dispatch(addMoreItem({ id, value }))
    }

    const onRemove = (id) => {
        dispatch(removeFromCart(id))
    }

    return (
        <List
            itemLayout="vertical"
            dataSource={items}
            locale={{
                emptyText: <Empty image={<ShoppingCartOutlined style={{ fontSize: 90 }} />} description="Your cart is empty" />
            }}
            footer={
                <div>
                    <div className={styles.flexBetween}>
                        <span>Subtotal</span>
                        <span>{subtotal} MMK</span>
                    </div>
                    <br />
                    <Button type="primary" block>Check Out</Button>
                </div>
            }
            renderItem={(item) => (
                <List.Item key={item.data.name}>
                    <List.Item.Meta
                        avatar={<img width={100} alt="logo" src={item.data.image} />}
                        title={
                            <div className={styles.flexBetween}>
                                <span>
                                    {item.data.name}
                                </span>
                                <span>
                                    {item.data.amount * item.count} MMK
                                </span>
                            </div>
                        }
                        description={
                            <Space direction="vertical" style={{ display: 'flex', }}>
                                <span>
                                    <UnorderedListOutlined /> {item.data.category.name}
                                </span>
                                <div className={styles.flexBetween}>
                                    <InputNumber min={1} max={10} defaultValue={item.count} onChange={(e) => onChange(e, item.data.id)} />
                                    <Button type="primary" danger onClick={() => onRemove(item.data.id)}>Remove</Button>
                                </div>
                            </Space>
                        }
                    />
                </List.Item>
            )}
        />
    );
}

export default Cart;