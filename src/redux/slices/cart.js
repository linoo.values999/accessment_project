import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    subtotal: 0,
    items: [],
}

const calTotal = (items) => {
    let total = 0;
    items.forEach(x => {
        total += (x.data.amount * x.count);
    })
    return total;
}

export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, action) => {
            let temp = [...state.items];
            let item = temp.find(x => x.data.id === action.payload.id);
            if (item) {
                temp = temp.map(x => {
                    if (x.data.id === action.payload.id) {
                        return { ...x, count: x.count + 1 }
                    }
                    return x;
                })
            } else {
                temp.push({
                    data: action.payload,
                    count: 1,
                })
            }

            state.items = temp;
            state.subtotal = calTotal(temp);
        },
        removeFromCart: (state, action) => {
            let temp = state.items.filter(x => {
                return x.data.id !== action.payload;
            });
            state.items = temp;
            state.subtotal = calTotal(temp);
        },
        addMoreItem: (state, action) => {
            let temp = state.items.map(x => {
                if (x.data.id === action.payload.id) {
                    return { ...x, count: action.payload.value }
                }
                return x;
            });
            state.items = temp;
            state.subtotal = calTotal(temp);
        }
    },
});

export const { addToCart, removeFromCart, addMoreItem } = cartSlice.actions

export default cartSlice.reducer