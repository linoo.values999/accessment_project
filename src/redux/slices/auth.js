import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    error: "",
    loading: false,
    auth: false,
}

export const authSlice = createSlice({
    name: 'authSlice',
    initialState,
    reducers: {
        load: (state) => {
            state.loading = !state.loading;
        },
        signIn: (state) => {
            state.auth = true;
            state.loading = false;
        },
        signOut: (state) => {
            state.auth = false;
            localStorage.clear();
        },
        showError: (state, action) => {
            console.log(action)
            state.error = action.payload
        }
    },
});

export const { signIn, signOut, load, showError } = authSlice.actions

export default authSlice.reducer