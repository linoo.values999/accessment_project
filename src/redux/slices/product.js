import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    searchName: "",
    loading: true,
    last: false,
    page: 0,
    size: 6,
    total: 0,
    list: [],
};

export const product = createSlice({
    name: 'product',
    initialState,
    reducers: {
        load: (state) => {
            state.loading = !state.loading;
            state.list = [];
        },
        fetchList: (state, actions) => {
            state.page += 1;
            state.list = actions.payload.content;
            state.total = actions.payload.totalElements;
            state.last = actions.payload.last;
            state.loading = false;
        },
        search: (state, action) => {
            state.searchName = action.payload;
        }
    },
});

export const { load, fetchList, search } = product.actions;

export default product.reducer;