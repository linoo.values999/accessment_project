import { message } from 'antd';
import { fetchList, load } from '../slices/product';

var headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

export const FetchProduct = ({ search, page = 0, size = 20 }) => (dispatch) => {
    var query = "";
    if (search) {
        query += `&name=${search}`
    }

    var token = localStorage.getItem("token");

    if (token) {
        headers = {
            ...headers,
            "Authorization": `Bearer ${token}`
        }
    }

    dispatch(load());

    fetch(`https://assessment-api.hivestage.com/api/products?page=${page}&size=${size}${query}`, {
        method: "GET",
        headers,
    }).then(res => res.json().then(response => {
        if (res.status === 200) {
            dispatch(fetchList(response));
        } else {
            dispatch(load());
            message.error(response.message)
        }
    }))
}