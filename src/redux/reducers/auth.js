import { message } from 'antd';
import { signIn, load, showError } from '../slices/auth';

var headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

export const SignIn = (data, callback) => (dispatch) => {
    dispatch(load());

    fetch("https://assessment-api.hivestage.com/api/auth/login", {
        body: JSON.stringify(data),
        method: "POST",
        headers,
    })
        .then(res => {
            res.json()
                .then(response => {
                    if (res.status === 200) {
                        localStorage.setItem("token", response.token)
                        dispatch(signIn());
                        callback();
                    } else {
                        dispatch(load());
                        dispatch(showError(response.message));
                        message.error(response.message)
                    }
                })
        })
}

export const Register = (data, callback) => (dispatch) => {
    dispatch(load());

    fetch("https://assessment-api.hivestage.com/api/auth/register", {
        body: JSON.stringify(data),
        method: "POST",
        headers,
    })
        .then(res => {
            res.json()
                .then(response => {
                    if (res.status === 200) {
                        localStorage.setItem("token", response.token)
                        dispatch(signIn());
                        callback();
                    } else {
                        dispatch(load());
                        dispatch(showError(response.message));
                        message.error(response.message)
                    }
                })
        })
}