import { configureStore } from '@reduxjs/toolkit'
import auth from './slices/auth';
import cart from './slices/cart';
import product from './slices/product';

export const store = configureStore({
    reducer: {
        authReducer: auth,
        productReducer: product,
        cartReducer: cart,
    },
})