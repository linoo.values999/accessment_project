/* eslint-disable react-hooks/exhaustive-deps */
import { Navigate, useLocation } from "react-router-dom";
import { useSelector } from 'react-redux'

export default function RequireAuth({ children }) {
    const { auth } = useSelector((state) => state.authReducer);

    let location = useLocation();

    if (!auth) {
        return <Navigate to="/login" state={{ from: location }} replace />;
    }

    return children;
}