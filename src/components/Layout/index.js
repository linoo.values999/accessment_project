import { Button, Layout, Drawer, Space, Input, Badge } from 'antd';
import { ShoppingCartOutlined } from '@ant-design/icons';
import styles from './index.module.less';
import React, { useState } from 'react';
import { Link, Outlet, useNavigate } from "react-router-dom";
import Cart from '../../pages/cart';
import { useSelector } from 'react-redux'
import logo from '../../logo.png';
import { useDispatch } from 'react-redux'
import { search } from '../../redux/slices/product'
import { signOut } from '../../redux/slices/auth'

const { Search } = Input;
const { Header, Content, Footer } = Layout;

const MainLayout = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const { items } = useSelector((state) => state.cartReducer);
    const { searchName } = useSelector((state) => state.productReducer);
    const { auth } = useSelector((state) => state.authReducer);

    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    const onLogout = () => {
        dispatch(signOut());
        navigate("/login");
    }

    return (
        <Layout className={styles.layout}>
            <Layout>
                <Header className={styles.header} >
                    <img src={logo} alt="logo" className={styles.logo} />
                    <Search
                        value={searchName}
                        style={{ width: 400 }}
                        placeholder="input search text"
                        allowClear
                        enterButton="Search"
                        onChange={(e) => dispatch(search(e.target.value))}
                    />
                    <Space size="large" >
                        <Link to="/">Shop</Link>
                        {auth ? <Button type='link' onClick={onLogout}>Logout</Button> : <Link to="/login">Login</Link>}
                        <Badge count={items.length} offset={[15, 15]}>
                            <Button type='link' shape="circle" icon={<ShoppingCartOutlined />} onClick={showDrawer} >
                                Cart
                            </Button>
                        </Badge>
                        {/* <Avatar>
                            A
                        </Avatar> */}
                    </Space>
                </Header>
                <Drawer width={500}
                    title="Shopping Cart"
                    placement="right"
                    closable={true}
                    onClose={onClose}
                    visible={visible}
                >
                    <Cart />
                </Drawer>
                <Content className={styles.contentRoot}>
                    <div className={styles.content}>
                        <Outlet />
                    </div>
                </Content>
                <Footer>
                    Ant Design ©2018 Created by Ant UED
                </Footer>
            </Layout>
        </Layout>
    );
}

export default MainLayout;